
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author gaiga
 */
public class TestReadFile {

    public static void main(String[] args) {
        Player o,x;
        o = new Player('o');
        x = new Player('x');
        File f = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            f = new File("ox.bin");
            fis = new FileInputStream(f);
            ois = new ObjectInputStream(fis);
            o = (Player) ois.readObject() ;
            x = (Player) ois.readObject() ;
            
        } catch (FileNotFoundException ex) {
            
        } catch (IOException ex) {
            
        } catch (ClassNotFoundException ex) {
           
        }
        System.out.println(o);
        System.out.println(x);
    }
}
