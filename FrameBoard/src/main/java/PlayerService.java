
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author gaiga
 */
public class PlayerService {

    static Player o, x;

    static {
        o = new Player('o');
        x = new Player('x');
    }

    public static void load() {
        File f = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            f = new File("ox.bin");
            fis = new FileInputStream(f);
            ois = new ObjectInputStream(fis);
            o = (Player) ois.readObject();
            x = (Player) ois.readObject();

        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        } catch (ClassNotFoundException ex) {
        }
        System.out.println(o);
        System.out.println(x);
    }

    public static void save() {
        System.out.println("Save");
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            File f = new File("ox.bin");
            fos = new FileOutputStream(f);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(o);
            oos.writeObject(x);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (oos != null) {
                    oos.close();
                }
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static Player getO() {
        return o;
    }

    public static Player getX() {
        return x;
    }

}
